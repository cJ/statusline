// SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-statusline@zougloub.eu>
// SPDX-License-Identifier: GPL-3.0-only

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#define DEFAULT_SOCKET_PATH "/tmp/statusline.sock"
#define BUFFER_SIZE 1024

int main() {
	int sockfd;
	int res;

	struct sockaddr_un server_addr, client_addr;
	char buffer[BUFFER_SIZE];

	// Get socket path from environment variable or use default
	const char *socket_path = getenv("STATUSLINE_SERVER_SOCKET");
	if (!socket_path) {
		socket_path = DEFAULT_SOCKET_PATH;
	}

	// Create socket
	if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	// Zero out the address structure
	memset(&server_addr, 0, sizeof(server_addr));

	// Set family and path
	server_addr.sun_family = AF_UNIX;
	//strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);

	client_addr.sun_family = AF_UNIX;

	char myaddr[256];
	sprintf(client_addr.sun_path, "/tmp/statusline-client-%d.sock", getpid());

	sprintf(server_addr.sun_path, "%s", socket_path);

	unlink(client_addr.sun_path);

	// Bind the socket to the address
	if (bind(sockfd, (struct sockaddr *)&client_addr, sizeof(client_addr)) < 0) {
		perror("bind failed");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	// Set up the server address
	server_addr.sun_family = AF_UNIX;
	strncpy(server_addr.sun_path, socket_path, sizeof(server_addr.sun_path) - 1);


	// Loop to receive data and write it to stdout
	while (1) {
		fd_set read_fds;
		FD_ZERO(&read_fds);
		FD_SET(sockfd, &read_fds);
		struct timeval timeout = {
			.tv_sec = 5,
			.tv_usec = 0,
		};

		res = select(sockfd + 1, &read_fds, NULL, NULL, &timeout);

		if (res < 0) {
			perror("select");
			close(sockfd);
			exit(EXIT_FAILURE);
		}
		else if (res == 0) {
			// Timeout occurred, send a message to the server
			char const * buffer = "Hello\n";
			// Send a message to the server
			while (sendto(sockfd, buffer, strlen(buffer), 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
				perror("sendto");
				sleep(1);
			}
		}
		else {
			ssize_t numRead = recv(sockfd, buffer, BUFFER_SIZE, 0);
			if (numRead > 0) {
				write(STDOUT_FILENO, buffer, numRead);
			}
		}
	}

	// Close socket (code will not reach here in this example)
	close(sockfd);
	return 0;
}

