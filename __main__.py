#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-statusline@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import socket
import time
import os
import select
import sys, io, os, re, subprocess, logging
import time
import types


from .sensors.battery import gauge_reader
from .sensors.hwmon import temp_reader
from .sensors.astronomical import sun_elevation


logger = logging.getLogger(__name__)


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(
	 description="Statusline server",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--socket",
	 default="/tmp/statusline.sock", # TODO use /run/user/{uid}/
	 help="Where to listen for clients",
	)

	parser.add_argument("elements",
	 nargs="*",
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)


	SOCKET_PATH = args.socket

	try:
		os.unlink(SOCKET_PATH)
	except OSError:
		if os.path.exists(SOCKET_PATH):
			raise

	def statusline_battery():
		gauge = gauge_reader()
		while True:
			v, v_full, eta = next(gauge)

			if eta == float('inf'):
				eta_s = "="
			elif eta is not None:
				eta_s = f"{eta/60:.1f}min"
			else:
				eta_s = "?"

			yield f"🔋{v/v_full*100:.3f}% {eta_s}"

	def statusline_analogclock():
		import datetime
		while True:
			now = datetime.datetime.now()
			value = int(round(((round((now.hour + now.minute / 60) * 2)/2) % 12) * 2))
			icons = "🕛🕧🕐🕜🕑🕝🕒🕞🕓🕟🕔🕠🕕🕡🕖🕢🕗🕣🕘🕤🕙🕥🕚🕦"
			yield icons[value]

	def statusline_coretemp():
		gauge = temp_reader()
		while True:
			temp = int(next(gauge))
			yield f"🌡🧠{temp}°C"

	def statusline_gputemp():
		gauge = temp_reader("thinkpad", "GPU")
		while True:
			temp = int(next(gauge))
			yield f"🌡📺{temp}°C"

	def position():
		# TODO gpsd?
		return float(os.environ.get("STATUSLINE_LATITUDE")), float(os.environ.get("STATUSLINE_LONGITUDE"))

	def statusline_sun_elevation():
		import datetime
		while True:
			now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
			latitude, longitude = position()

			elevation = sun_elevation(now, latitude, longitude)
			if elevation < -18: # night
				icon = "🌃"
			elif elevation < -12: # astronomical twilight
				icon = "🌃"
			elif elevation < -6: # nautical twilight
				icon = "🌃"
			elif elevation < -0.2656: # civil twilight
				icon = "🌆"
			elif elevation < 0.2656: # sunset
				icon = "🌇"
			else: # day
				icon = "🏙"
			yield f"{icon}({elevation:.3f})"

	def statusline_counter():
		i = 0
		while True:
			yield f"{i}"
			i += 1

	clients = set()

	elements = list()

	for spec in args.elements:
		logger.info("Adding %s", spec)

		f = locals().get(f"statusline_{spec}")
		if f is None:
			elements.append(spec)
		else:
			elements.append(f())


	with socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM) as sock:
		sock.bind(SOCKET_PATH)
		while True:

			timeout = 1.0
			rs = [sock]
			ws = []
			xs = []
			rs, ws, xs = select.select(rs, ws, xs, timeout)

			if rs:
				data, client_address = sock.recvfrom(1024)
				logger.info("Client %s said %s", client_address, data)
				clients.add(client_address)
			else:

				linechunks = []
				for element in elements:
					if isinstance(element, str):
						linechunks.append(element)
					else:
						linechunks.append(next(element))

				line = (" ".join(linechunks) + "\n").encode("utf-8")

				torem = set()

				for client_address in clients:
					try:
						sock.sendto(line, client_address)
					except socket.error as e:
						logger.info("Error sending data: %s", e)
						torem.add(client_address)

				if torem:
					clients.difference_update(torem)

