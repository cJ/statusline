#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-statusline@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import io
import os
import time


from .sysfs import sysfs_slurper


def reader(path=None, element="power_now", conversion=int):
	"""
	"""
	if path is None:
		path = "/sys/class/power_supply/BAT0"

	path = os.path.join(path, element)
	for value in sysfs_slurper(path):
		if value is None:
			yield
		else:
			yield conversion(value)

def voltage_reader(path=None):
	yield from reader(path=path, element="voltage_now")

def power_reader(path=None):
	yield from reader(path=path, element="power_now")



def gauge_reader(path=None):
	"""
	:return: yield energy now, energy full, and eta to charge or discharge
	"""

	last_time = None
	last_energy = None

	if path is None:
		path = "/sys/class/power_supply/BAT0"

	while True:
		while not os.path.exists(path):
			time.sleep(1)

		with io.open(path + "/energy_full", "r", encoding="utf-8") as fi:
			energy_full = int(fi.read())# * 1e-6

		with io.open(path + "/energy_now", "r", encoding="utf-8") as fi:
			while True:
				try:
					fi.seek(0)
					energy_now = int(fi.read())# * 1e-6
				except:
					break

				current_time = time.time()

				if last_time is not None and last_energy is not None:
					time_diff = current_time - last_time
					energy_diff = energy_now - last_energy

					rate = energy_diff / time_diff
					if rate != 0:
						if energy_diff > 0:
							eta = (energy_full - energy_now) / rate
						else:
							eta = -energy_now / rate
					else:
						eta = float('inf')  # Rate is zero, cannot estimate ETA
				else:
					eta = None

				last_energy = energy_now
				last_time = current_time
				yield energy_now, energy_full, eta

