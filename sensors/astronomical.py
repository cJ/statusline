#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-statusline@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import logging
import datetime
from math import (
 sin,
 cos,
 acos,
 asin,
 tan,
 pi,
)


logger = logging.getLogger(__name__)


def Mod(x, y):
	return x % y

julian_origin = datetime.datetime(
 year=2000, month=1, day=1,
 hour=11, minute=58, second=55,
 tzinfo=datetime.timezone.utc,
)


def sun_elevation(now: datetime.datetime, latitude, longitude):
	"""
	:return: Sun elevation in °
	:param now: UTC date/time (with tzinfo)
	:param latitude: latitude in °
	:param longitude: longitude in °
	"""
	julian_century = (now - julian_origin).total_seconds() / 3155760000
	LTOD = (now.hour * 3600 + now.minute * 60 + now.second) / (24 * 60 * 60)
	x0 = 0.0174532925199433*latitude
	x1 = 33.757041381353*julian_century - 2.18235969669371
	x2 = cos(x1)
	x3 = julian_century*(5.03611111111111e-7*julian_century - 1.63888888888889e-7) - 0.0130041666666667
	x4 = 0.0174532925199433*julian_century
	x5 = sin(4.46804288510548e-5*x2 + x3*x4 + 0.409092804222329)
	x7 = 35999.05029 - 0.0001537*julian_century
	x8 = julian_century*x7
	x9 = sin(0.0349065850398866*x8 + 12.48012028245)
	x10 = sin(x4*x7 + 6.24006014122498)
	x6 = 0.0003032028*julian_century + 36000.76982779
	x11 = sin(0.0174532925199433*x10*(-julian_century*(1.4e-5*julian_century + 0.004817) + 1.914602) + x4*x6 + 0.0174532925199433*x9*(0.019993 - 0.000101*julian_century) + 8.3426738245329e-5*sin(x1) + 5.04400153826361e-6*sin(0.0523598775598299*x8 + 18.720180423675) + 4.89496380158267)
	x12 = -julian_century*(1.267e-7*julian_century + 4.2037e-5) + 0.016708634
	x13 = x10*x12
	x14 = julian_century*x6
	x15 = 0.0349065850398866*x14 + 9.79012622163422
	x16 = tan(0.00872664625997165*julian_century*x3 + 2.23402144255274e-5*x2 + 0.204546402111164)
	x17 = x16**2
	x18 = acos(x11*x5*sin(x0) + (-x11**2*x5**2 + 1)**0.5*cos(x0)*cos(0.00436332312998582*(Mod(1440.0*LTOD + 4.0*longitude - 286.478897565412*x12**2*x9 + 916.732472209317*x13*x17*cos(x15) - 458.366236104659*x13 - 114.591559026165*x16**4*sin(0.0698131700797732*x14 + 19.5802524432684) + 229.183118052329*x17*sin(x15), 1440.0)) - 3.14159265358979))
	x19 = 57.2957795130823*x18
	x20 = 90.0 - x19
	return x20 + 0.017/tan((x20 + 10.3/(x20 + 5.11))*(pi/180))
