#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-statusline@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import io
import os

def sysfs_slurper(path):
	"""
	"""
	while True:
		while not os.path.exists(path):
			yield

		with io.open(path, "r", encoding="utf-8") as fi:
			while True:
				try:
					fi.seek(0)
					value = fi.read()
				except:
					break

				yield value
