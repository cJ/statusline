#!/usr/bin/env python

# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostislavlobov.k@gmail.com>
# SPDX-License-Identifier: GPL-3.0-only

import typing as t
import ctypes
from ctypes import c_int, POINTER
from ctypes.util import find_library


try:
	lib_name = find_library("libbattery_info")
	lib = ctypes.CDLL(lib_name)
except OSError:
	raise Exception("Lib not found (https://gitlab.com/rostislavlobov.k/battery-info-osx)")

lib.get_battery_data.argtypes = [POINTER(c_int), POINTER(c_int), POINTER(c_int)]


def gauge_reader():
	voltage_mV = c_int()
	current_mA = c_int()
	temp_cC = c_int()
	while True:
		lib.get_battery_data(ctypes.byref(voltage_mV), ctypes.byref(current_mA), ctypes.byref(temp_cC))
		yield voltage_mV.value, current_mA.value, None


def get_battery_data() -> t.Tuple[float, float, float]:
	"""
	:return: Voltage, Current, and Temprature in float
	"""
	voltage_mV = c_int()
	current_mA = c_int()
	temp_cC = c_int()
	lib.get_battery_data(ctypes.byref(voltage_mV), ctypes.byref(current_mA), ctypes.byref(temp_cC))
	return (voltage_mV.value / 1000), (current_mA.value / 1000), (temp_cC.value / 100)
