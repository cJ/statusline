#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-statusline@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import io
import os
import time
import logging


logger = logging.getLogger(__name__)


def find_path(sensor, tp, name):
	hwmon = "/sys/class/hwmon"
	for device in os.listdir(hwmon):

		logger.debug("Checking sensor device %s", device)

		root = os.path.join(hwmon, device)
		p = os.path.join(root, "name")
		with io.open(p, "r") as fi:
			sensor_ = fi.read().strip()
		if sensor_ != sensor:
			continue

		logger.debug("Found path for sensor: %s", root)

		for i in range(1, 1000):
			p = os.path.join(root, f"{tp}{i}_")
			logger.debug("Checking %s", p)
			if not os.path.exists(p + "label"):
				break
			with io.open(p + "label", "r") as fi:
				name_ = fi.read().strip()
				if name_ == name:
					return p + "input"


def temp_reader(name="coretemp", temperature_name="Package id 0"):
	"""
	:return: yield temperature
	"""

	while True:
		while (path := find_path(name, "temp", temperature_name)) is None:
			time.sleep(1)

		with io.open(path, "r", encoding="utf-8") as fi:
			while True:
				try:
					fi.seek(0)
					temp = int(fi.read()) / 1000
				except:
					break

				yield temp
