##########
statusline
##########

statusline is a solution for obtaining a **system status** text line,
similar to powerline.

It differs from it in that it places emphasis on the fact that the
status line is updated regularly, displayed in several places, and it
should be relatively cheap to obtain it.

Thus, it is using a client-server architecture, where the server is
computing the status line, and the (lightweight, C) clients are
retrieving it.


Usage
#####


The server must be running. Then the clients connect to it, obtain
the status line, and print it.

To start the server:

.. code:: sh

   python -m cJ.statusline battery coretemp gputemp 😱 analogclock sun_elevation

Then the client:

.. code:: sh

   statusline

Which will display for example::

  🔋94.342%= 🌡🧠95°C 🌡📺50°C 😱 🕤 🏙
  🔋94.343%= 🌡🧠95°C 🌡📺50°C 😱 🕤 🏙
  ...


Frugal Computing
################

- The client is a long-living program, printing one line per update
- The server avoids running subprocesses, or doing anything that could
  be wasteful, for each update
- Nevertheless the server is implemented in Python, for ease of
  development.

